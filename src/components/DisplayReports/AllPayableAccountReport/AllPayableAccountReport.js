import React from "react";

import "./AllPayableAccountReport.css";

const AllPayableAccountReport = () => {
  return (
    <div className="mainPayableContainer">
      <h1>All Payable Account Report</h1>
      <div className="payableBodyContainer">
      <div className="payableTableContainer">
          <table className="table">
            <thead className="thead-dark">
              <tr>
                <th scope="col">#</th>
                <th scope="col">Parties Name</th>
                <th scope="col">Total Debit</th>
                <th scope="col">Total Credit</th>
                <th scope="col">Balance Amount</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <th scope="row">1</th>
                <td>Mark</td>
                <td>Rs100000</td>
                <td>Rs250000</td>
                <td>Rs300000</td>
              </tr>
            </tbody>
          </table>
        </div>
        <div className="totalAmountStyle">
              <div className="totalAmountTextStyle">Total Amount:</div>
              <input type="text" className="totalAmountInputField"></input>
        </div>
      </div>
      <div className="payableFooter">
        <button className="printPageButtonStyle">Print Page</button>
          <button className="cancelButtonStyle">Cancel</button>
      </div>
    </div>
  )
  
};

export default AllPayableAccountReport;
