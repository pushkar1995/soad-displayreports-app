import React, { Component } from "react";

import { Link } from 'react-router-dom';

export default class DisplayReportsPage extends Component {
  render() {
    return (
      <div className="displayReportsContainer">
        <div className="displayReportsContent">
          <Link to="/allReceivableAccountReport" className="nav-link">
            <button className="receivableButtonStyle">All Receivable Accounts</button>
          </Link>
          
          <Link to="/allPayableAccountReport">
            <button className="payableButtonStyle">All Payable Accounts</button>
          </Link>
         
          <Link to="closingStockReport">
          <button className="stockReportButtonStyle">Closing Stock Report</button>
          </Link>
          

          <button className="vatButtonStyle">Vat Details</button>
          <button className="vatButtonStyle">Transactions Entry</button>
        </div>
      </div>
    )
   
  }
}
