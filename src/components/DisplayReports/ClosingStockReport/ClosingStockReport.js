import React from "react";

import "./ClosingStockReport.css";

const ClosingStockReport = () => {
  return (
    <div className="mainstockReportContainer">
    <h1>Closing Stock Report</h1>
    <div className="stockReportBodyContainer">
    <div className="stockReportTableContainer">
        <table className="table">
          <thead className="thead-dark">
            <tr>
              <th scope="col">#</th>
              <th scope="col">Item Code</th>
              <th scope="col">Item Name</th>
              <th scope="col">Opening Stock</th>
              <th scope="col">Purchase</th>
              <th scope="col">Sales</th>
              <th scope="col">Closing Stock</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <th scope="row">1</th>
              <td>Mark</td>
              <td>Rs100000</td>
              <td>Rs250000</td>
              <td>Rs300000</td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
    <div className="payableFooter">
      <button className="printPageButtonStyle">Print Stock Report</button>
        <button className="cancelButtonStyle">Cancel</button>
    </div>
  </div>
  )
};

export default ClosingStockReport;
