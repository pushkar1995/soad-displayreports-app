import React from 'react';

import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';

import DisplayReportPage from './DisplayReports/DisplayReportsPage/DisplayReportsPage';
import AllReceivableAccountReport from './DisplayReports/AllReceivableAccountReport/AllReceivableAccountReport';
import AllPayableAccountReport from './DisplayReports/AllPayableAccountReport/AllPayableAccountReport';
import ClosingStockReport from './DisplayReports/ClosingStockReport/ClosingStockReport';


const App = () => {
    return (
            <Router>
                <div className="App">
                <div className="container">
                <Switch>
                    <Route exact path="/" component={DisplayReportPage} />
                    <Route exact path="/allReceivableAccountReport" component={AllReceivableAccountReport} />
                    <Route exact path="/allPayableAccountReport" component={AllPayableAccountReport} />
                    <Route exact path="/closingStockReport" component={ClosingStockReport} />                
                </Switch>
                </div>
                </div>
            </Router>   
    )
} 

export default App;